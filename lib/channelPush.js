"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.channelPush = void 0;
function channelPush(channel, event, payload, timeout) {
    return new Promise(function (resolve, reject) {
        return channel
            .push(event, payload, timeout)
            .receive("ok", resolve)
            .receive("error", reject);
    });
}
exports.channelPush = channelPush;
