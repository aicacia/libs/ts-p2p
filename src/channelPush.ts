import { Channel } from "phoenix";

export function channelPush(
  channel: Channel,
  event: string,
  payload: any,
  timeout?: number
): Promise<void> {
  return new Promise((resolve, reject) =>
    channel
      .push(event, payload, timeout)
      .receive("ok", resolve)
      .receive("error", reject)
  );
}
