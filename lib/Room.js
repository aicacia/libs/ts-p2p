"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Room = exports.RoomEventType = exports.RoomInEventType = exports.RoomOutEventType = void 0;
var tslib_1 = require("tslib");
var core_1 = require("@aicacia/core");
var events_1 = require("events");
var channelPush_1 = require("./channelPush");
var RoomOutEventType;
(function (RoomOutEventType) {
    RoomOutEventType["BROADCAST"] = "broadcast";
    RoomOutEventType["SEND_TO"] = "send_to";
    RoomOutEventType["UPDATE_PEER_DATA"] = "update_peer_data";
    RoomOutEventType["DISCOVER_PEERS"] = "discover_peers";
    RoomOutEventType["SEND_PEER_TO"] = "send_peer_to";
    RoomOutEventType["SEND_DISCOVERED_PEER_TO"] = "send_discovered_peer_to";
})(RoomOutEventType = exports.RoomOutEventType || (exports.RoomOutEventType = {}));
var RoomInEventType;
(function (RoomInEventType) {
    RoomInEventType["PEER_JOIN"] = "peer_join";
    RoomInEventType["PEER_LEAVE"] = "peer_leave";
    RoomInEventType["PEER_DATA_UPDATED"] = "peer_data_updated";
    RoomInEventType["MESSAGE"] = "message";
    RoomInEventType["MESSAGE_TO"] = "message_to";
    RoomInEventType["REQUEST_PEER"] = "request_peer";
    RoomInEventType["DISCOVERED_PEER"] = "discovered_peer";
})(RoomInEventType = exports.RoomInEventType || (exports.RoomInEventType = {}));
var RoomEventType;
(function (RoomEventType) {
    RoomEventType["JOIN"] = "join";
    RoomEventType["LEAVE"] = "leave";
    RoomEventType["PEER_JOIN"] = "peer_join";
    RoomEventType["PEER_LEAVE"] = "peer_leave";
    RoomEventType["PEER_DATA_UPDATED"] = "peer_data_updated";
    RoomEventType["MESSAGE"] = "message";
})(RoomEventType = exports.RoomEventType || (exports.RoomEventType = {}));
var Room = /** @class */ (function (_super) {
    tslib_1.__extends(Room, _super);
    function Room(socket, channel, name, data) {
        var _this = _super.call(this) || this;
        _this.peers = {};
        _this.joined = false;
        _this.socket = socket;
        _this.name = name;
        _this.channel = channel;
        _this.data = data;
        _this.channel.on(RoomInEventType.REQUEST_PEER, function (_a) {
            var requestor_id = _a.requestor_id;
            channelPush_1.channelPush(_this.channel, RoomOutEventType.SEND_PEER_TO, {
                requestor_id: requestor_id,
                peer_id: _this.getId(),
                data: _this.getData(),
            });
        });
        _this.channel.on(RoomInEventType.DISCOVERED_PEER, function (_a) {
            var requestor_id = _a.requestor_id, peer_id = _a.peer_id, data = _a.data;
            channelPush_1.channelPush(_this.channel, RoomOutEventType.SEND_DISCOVERED_PEER_TO, {
                requestor_id: requestor_id,
                peer_id: peer_id,
                data: data,
            });
        });
        _this.channel.on(RoomInEventType.PEER_DATA_UPDATED, function (_a) {
            var peer_id = _a.peer_id, data = _a.data;
            _this.peers[peer_id] = data;
            _this.emit(RoomEventType.PEER_DATA_UPDATED, peer_id, data);
        });
        _this.channel.on(RoomInEventType.PEER_JOIN, function (_a) {
            var peer_id = _a.peer_id, data = _a.data;
            _this.peers[peer_id] = data;
            if (_this.hasPeer(peer_id)) {
                _this.emit(RoomEventType.PEER_JOIN, peer_id, data);
            }
        });
        _this.channel.on(RoomInEventType.PEER_LEAVE, function (_a) {
            var peer_id = _a.peer_id;
            var peer = _this.peers[peer_id];
            if (peer) {
                _this.emit(RoomEventType.PEER_LEAVE, peer_id, peer);
                delete _this.peers[peer_id];
            }
        });
        _this.channel.on(RoomInEventType.MESSAGE, function (_a) {
            var from_id = _a.from_id, payload = _a.payload;
            _this.emit(RoomEventType.MESSAGE, from_id, payload);
        });
        return _this;
    }
    Room.prototype.getId = function () {
        return this.socket.getId();
    };
    Room.prototype.getData = function () {
        return this.data;
    };
    Room.prototype.getPeerData = function (peer_id) {
        if (this.hasPeer(peer_id)) {
            return core_1.some(this.peers[peer_id]);
        }
        else {
            return core_1.none();
        }
    };
    Room.prototype.hasPeer = function (peer_id) {
        return this.peers.hasOwnProperty(peer_id);
    };
    Room.prototype.getPeers = function () {
        return this.peers;
    };
    Room.prototype.getPhoenixChannel = function () {
        return this.channel;
    };
    Room.prototype.getName = function () {
        return this.name;
    };
    Room.prototype.join = function (timeout) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!this.joined) return [3 /*break*/, 2];
                        this.joined = true;
                        return [4 /*yield*/, new Promise(function (resolve, reject) {
                                _this.channel
                                    .join(timeout)
                                    .receive("ok", function () {
                                    channelPush_1.channelPush(_this.channel, RoomOutEventType.DISCOVER_PEERS, {});
                                    _this.emit(RoomEventType.JOIN);
                                    resolve();
                                })
                                    .receive("error", reject);
                            }).catch(function (error) {
                                _this.joined = false;
                                throw error;
                            })];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [2 /*return*/, this];
                }
            });
        });
    };
    Room.prototype.leave = function (timeout) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.joined) return [3 /*break*/, 2];
                        this.joined = false;
                        return [4 /*yield*/, new Promise(function (resolve, reject) {
                                return _this.channel
                                    .leave(timeout)
                                    .receive("ok", function () {
                                    _this.emit(RoomEventType.LEAVE);
                                    resolve();
                                })
                                    .receive("error", reject);
                            })];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [2 /*return*/, this];
                }
            });
        });
    };
    Room.prototype.setData = function (data) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.data = data;
                        return [4 /*yield*/, channelPush_1.channelPush(this.channel, RoomOutEventType.UPDATE_PEER_DATA, data)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, this];
                }
            });
        });
    };
    Room.prototype.broadcast = function (payload) {
        return channelPush_1.channelPush(this.channel, RoomOutEventType.BROADCAST, payload);
    };
    Room.prototype.sendTo = function (peer_id, payload) {
        return channelPush_1.channelPush(this.channel, RoomOutEventType.SEND_TO, {
            from_id: this.getId(),
            to_id: peer_id,
            payload: payload,
        });
    };
    return Room;
}(events_1.EventEmitter));
exports.Room = Room;
