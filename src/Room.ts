import { none, Option, some } from "@aicacia/core";
import { EventEmitter } from "events";
import { Channel } from "phoenix";
import { channelPush } from "./channelPush";

export enum RoomOutEventType {
  BROADCAST = "broadcast",
  SEND_TO = "send_to",
  UPDATE_PEER_DATA = "update_peer_data",
  DISCOVER_PEERS = "discover_peers",
  SEND_PEER_TO = "send_peer_to",
  SEND_DISCOVERED_PEER_TO = "send_discovered_peer_to",
}

export enum RoomInEventType {
  PEER_JOIN = "peer_join",
  PEER_LEAVE = "peer_leave",
  PEER_DATA_UPDATED = "peer_data_updated",
  MESSAGE = "message",
  MESSAGE_TO = "message_to",
  REQUEST_PEER = "request_peer",
  DISCOVERED_PEER = "discovered_peer",
}

export enum RoomEventType {
  JOIN = "join",
  LEAVE = "leave",
  PEER_JOIN = "peer_join",
  PEER_LEAVE = "peer_leave",
  PEER_DATA_UPDATED = "peer_data_updated",
  MESSAGE = "message",
}

// tslint:disable-next-line: interface-name
export declare interface Room<Payload = any, Data = any> {
  on(
    event: RoomEventType.JOIN | RoomEventType.LEAVE,
    listener: (this: Room<Payload, Data>) => void
  ): this;
  on(
    event: RoomEventType.PEER_JOIN | RoomEventType.PEER_LEAVE,
    listener: (this: Room<Payload, Data>, peer_id: string, data: Data) => void
  ): this;
  on(
    event: RoomEventType.PEER_DATA_UPDATED,
    listener: (this: Room<Payload, Data>, peer_id: string, data: Data) => void
  ): this;
  on(
    event: RoomEventType.MESSAGE,
    listener: (
      this: Room<Payload, Data>,
      fromId: string,
      payload: Payload
    ) => void
  ): this;
}

export class Room<Payload = any, Data = any> extends EventEmitter {
  private socket: Socket;
  private name: string;
  private channel: Channel;
  private data: Data;
  private peers: Record<string, Data> = {};
  private joined = false;

  constructor(socket: Socket, channel: Channel, name: string, data: Data) {
    super();
    this.socket = socket;
    this.name = name;
    this.channel = channel;
    this.data = data;

    this.channel.on(RoomInEventType.REQUEST_PEER, ({ requestor_id }) => {
      channelPush(this.channel, RoomOutEventType.SEND_PEER_TO, {
        requestor_id,
        peer_id: this.getId(),
        data: this.getData(),
      });
    });
    this.channel.on(
      RoomInEventType.DISCOVERED_PEER,
      ({ requestor_id, peer_id, data }) => {
        channelPush(this.channel, RoomOutEventType.SEND_DISCOVERED_PEER_TO, {
          requestor_id,
          peer_id,
          data,
        });
      }
    );
    this.channel.on(RoomInEventType.PEER_DATA_UPDATED, ({ peer_id, data }) => {
      this.peers[peer_id] = data;
      this.emit(RoomEventType.PEER_DATA_UPDATED, peer_id, data);
    });
    this.channel.on(RoomInEventType.PEER_JOIN, ({ peer_id, data }) => {
      this.peers[peer_id] = data;

      if (this.hasPeer(peer_id)) {
        this.emit(RoomEventType.PEER_JOIN, peer_id, data);
      }
    });
    this.channel.on(RoomInEventType.PEER_LEAVE, ({ peer_id }) => {
      const peer = this.peers[peer_id];

      if (peer) {
        this.emit(RoomEventType.PEER_LEAVE, peer_id, peer);
        delete this.peers[peer_id];
      }
    });
    this.channel.on(RoomInEventType.MESSAGE, ({ from_id, payload }) => {
      this.emit(RoomEventType.MESSAGE, from_id, payload);
    });
  }

  getId() {
    return this.socket.getId();
  }
  getData() {
    return this.data;
  }

  getPeerData(peer_id: string): Option<Data> {
    if (this.hasPeer(peer_id)) {
      return some(this.peers[peer_id]);
    } else {
      return none();
    }
  }
  hasPeer(peer_id: string) {
    return this.peers.hasOwnProperty(peer_id);
  }
  getPeers() {
    return this.peers;
  }

  getPhoenixChannel() {
    return this.channel;
  }
  getName() {
    return this.name;
  }

  async join(timeout?: number) {
    if (!this.joined) {
      this.joined = true;
      await new Promise((resolve, reject) => {
        this.channel
          .join(timeout)
          .receive("ok", () => {
            channelPush(this.channel, RoomOutEventType.DISCOVER_PEERS, {});
            this.emit(RoomEventType.JOIN);
            resolve();
          })
          .receive("error", reject);
      }).catch((error) => {
        this.joined = false;
        throw error;
      });
    }
    return this;
  }

  async leave(timeout?: number) {
    if (this.joined) {
      this.joined = false;
      await new Promise((resolve, reject) =>
        this.channel
          .leave(timeout)
          .receive("ok", () => {
            this.emit(RoomEventType.LEAVE);
            resolve();
          })
          .receive("error", reject)
      );
    }
    return this;
  }

  async setData(data: Data) {
    this.data = data;
    await channelPush(this.channel, RoomOutEventType.UPDATE_PEER_DATA, data);
    return this;
  }

  broadcast(payload: Payload) {
    return channelPush(this.channel, RoomOutEventType.BROADCAST, payload);
  }

  sendTo(peer_id: string, payload: Payload) {
    return channelPush(this.channel, RoomOutEventType.SEND_TO, {
      from_id: this.getId(),
      to_id: peer_id,
      payload,
    });
  }
}

import { Socket } from "./Socket";
