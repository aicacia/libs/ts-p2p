"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseAppId = void 0;
var RE_APP_ID = /[^A-Za-z0-9_\-\.]+/gm;
function parseAppId(appId) {
    return appId.replace(RE_APP_ID, "");
}
exports.parseAppId = parseAppId;
