/// <reference types="node" />
import { Option } from "@aicacia/core";
import { EventEmitter } from "events";
import { Channel } from "phoenix";
export declare enum RoomOutEventType {
    BROADCAST = "broadcast",
    SEND_TO = "send_to",
    UPDATE_PEER_DATA = "update_peer_data",
    DISCOVER_PEERS = "discover_peers",
    SEND_PEER_TO = "send_peer_to",
    SEND_DISCOVERED_PEER_TO = "send_discovered_peer_to"
}
export declare enum RoomInEventType {
    PEER_JOIN = "peer_join",
    PEER_LEAVE = "peer_leave",
    PEER_DATA_UPDATED = "peer_data_updated",
    MESSAGE = "message",
    MESSAGE_TO = "message_to",
    REQUEST_PEER = "request_peer",
    DISCOVERED_PEER = "discovered_peer"
}
export declare enum RoomEventType {
    JOIN = "join",
    LEAVE = "leave",
    PEER_JOIN = "peer_join",
    PEER_LEAVE = "peer_leave",
    PEER_DATA_UPDATED = "peer_data_updated",
    MESSAGE = "message"
}
export declare interface Room<Payload = any, Data = any> {
    on(event: RoomEventType.JOIN | RoomEventType.LEAVE, listener: (this: Room<Payload, Data>) => void): this;
    on(event: RoomEventType.PEER_JOIN | RoomEventType.PEER_LEAVE, listener: (this: Room<Payload, Data>, peer_id: string, data: Data) => void): this;
    on(event: RoomEventType.PEER_DATA_UPDATED, listener: (this: Room<Payload, Data>, peer_id: string, data: Data) => void): this;
    on(event: RoomEventType.MESSAGE, listener: (this: Room<Payload, Data>, fromId: string, payload: Payload) => void): this;
}
export declare class Room<Payload = any, Data = any> extends EventEmitter {
    private socket;
    private name;
    private channel;
    private data;
    private peers;
    private joined;
    constructor(socket: Socket, channel: Channel, name: string, data: Data);
    getId(): string;
    getData(): Data;
    getPeerData(peer_id: string): Option<Data>;
    hasPeer(peer_id: string): boolean;
    getPeers(): Record<string, Data>;
    getPhoenixChannel(): Channel;
    getName(): string;
    join(timeout?: number): Promise<this>;
    leave(timeout?: number): Promise<this>;
    setData(data: Data): Promise<this>;
    broadcast(payload: Payload): Promise<void>;
    sendTo(peer_id: string, payload: Payload): Promise<void>;
}
import { Socket } from "./Socket";
