import { Socket, Room, RoomEventType } from "../../src";

const WS_URL = "wss://api.p2p.aicacia.com";
const STUN_URL = "stun://stun.l.google.com:19302";

function createUuid() {
  return Math.random().toString(36).slice(2);
}

function onLoad() {
  const ROOM_ID = "room",
    JOIN_ROOM_ID = "join-room",
    ROOM_INPUT_ID = "room-input",
    JOIN_SUBMIT_ID = "join-submit",
    CREATE_ROOM_ID = "create-room",
    LEAVE_ROOM_ID = "leave-room",
    ROOM_TEXT_ID = "room-text",
    PEER_NAME_ID = "peer-name",
    NAME_INPUT_ID = "name-input",
    NAME_SUBMIT_ID = "name-submit";

  const socket = new Socket(createUuid(), {
    url: `${WS_URL}/socket`,
    appId: "p2p-simple-example",
  });

  (window as any).socket = socket;

  socket.connect().then(() => {
    console.log("Connected");
  });

  function initJoinRoomForm() {
    const form = document.getElementById(JOIN_ROOM_ID) as HTMLFormElement,
      input = document.getElementById(ROOM_INPUT_ID) as HTMLInputElement,
      nameInput = document.getElementById(NAME_INPUT_ID) as HTMLInputElement,
      submit = document.getElementById(JOIN_SUBMIT_ID) as HTMLInputElement;

    submit.onsubmit = (e) => {
      e.preventDefault();
    };
    form.onsubmit = (e) => {
      e.preventDefault();
      socket.joinRoom(input.value, { name: nameInput.value }).then(onJoin);
    };
  }

  function initPeerNameForm(room: Room) {
    const form = document.getElementById(PEER_NAME_ID) as HTMLFormElement,
      input = document.getElementById(NAME_INPUT_ID) as HTMLInputElement,
      submit = document.getElementById(NAME_SUBMIT_ID) as HTMLInputElement;

    submit.onsubmit = (e) => {
      e.preventDefault();
    };
    form.onsubmit = (e) => {
      e.preventDefault();
      room.setData({ name: input.value });
    };
  }

  function initCreateRoom() {
    const createRoom = document.getElementById(
        CREATE_ROOM_ID
      ) as HTMLButtonElement,
      input = document.getElementById(NAME_INPUT_ID) as HTMLInputElement;

    createRoom.onclick = () => {
      socket.joinRoom(createUuid(), { name: input.value }).then(onJoin);
    };
  }

  function initLeaveRoom(room: Room) {
    const createRoom = document.getElementById(
      LEAVE_ROOM_ID
    ) as HTMLButtonElement;

    createRoom.onclick = () => {
      room.leave().then(hideRoom);
    };
  }

  function showRoom(room: Room) {
    const roomElement = document.getElementById(ROOM_ID) as HTMLDivElement,
      roomText = document.getElementById(ROOM_TEXT_ID) as HTMLParagraphElement;

    roomElement.style.display = "block";
    roomText.textContent = room.getName();
  }

  function hideRoom() {
    const room = document.getElementById(ROOM_ID) as HTMLDivElement,
      roomText = document.getElementById(ROOM_TEXT_ID) as HTMLParagraphElement;

    room.style.display = "hidden";
    roomText.textContent = "";
  }

  function onJoin(room: Room) {
    console.log("Joined");

    showRoom(room);
    initLeaveRoom(room);
    initPeerNameForm(room);

    room.on(RoomEventType.PEER_LEAVE, (peerId) => {
      console.log(`Peer<${peerId}> left`);
    });
    room.on(RoomEventType.PEER_DATA_UPDATED, (peerId, data) => {
      console.log(`Peer<${peerId}> updated data`, data);
    });
    room.on(RoomEventType.MESSAGE, (fromId, payload) =>
      console.log(`Got message from ${fromId}`, payload)
    );
    room.on(RoomEventType.PEER_JOIN, (peerId, data) => {
      console.log(`Peer<${peerId}> joined`, data);
      startWebRTC(room, true);
    });
  }

  function startWebRTC(room: Room, isOfferer) {
    const pc = new RTCPeerConnection({
      iceServers: [
        {
          urls: STUN_URL,
        },
      ],
    });

    pc.addEventListener("icecandidate", (event) => {
      if (event.candidate) {
        room.broadcast({ candidate: event.candidate }).catch(onError);
      }
    });

    if (isOfferer) {
      pc.addEventListener("negotiationneeded", () => {
        pc.createOffer().then(localDescCreated).catch(onError);
      });
    }

    pc.addEventListener("track", (event) => {
      const remoteVideo = document.getElementById(
        "remote-video"
      ) as HTMLVideoElement;
      remoteVideo.srcObject = event.streams[0];
    });

    navigator.mediaDevices
      .getUserMedia({
        audio: false,
        video: true,
      })
      .then((stream) => {
        pc.addTrack(stream.getVideoTracks()[0], stream);
      }, onError);

    function localDescCreated(desc) {
      return pc
        .setLocalDescription(desc)
        .then(() => room.broadcast({ sdp: pc.localDescription }))
        .catch(onError);
    }

    function onError(error) {
      console.error(error);
    }

    room.on(RoomEventType.MESSAGE, (_peerId, payload) => {
      if (payload.sdp) {
        pc.setRemoteDescription(new RTCSessionDescription(payload.sdp))
          .then(() => {
            if (pc.remoteDescription.type === "offer") {
              pc.createAnswer().then(localDescCreated).catch(onError);
            }
          })
          .catch(onError);
      } else if (payload.candidate) {
        pc.addIceCandidate(new RTCIceCandidate(payload.candidate)).catch(
          onError
        );
      }
    });
  }

  initJoinRoomForm();
  initCreateRoom();
}

window.addEventListener("load", onLoad);
