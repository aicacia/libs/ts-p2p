"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Socket = exports.SocketEventType = void 0;
var tslib_1 = require("tslib");
var core_1 = require("@aicacia/core");
var events_1 = require("events");
var parseAppId_1 = require("./parseAppId");
var phoenix_1 = require("phoenix");
var Room_1 = require("./Room");
var SocketEventType;
(function (SocketEventType) {
    SocketEventType["CONNECT"] = "connect";
    SocketEventType["DISCONNECT"] = "disconnect";
})(SocketEventType = exports.SocketEventType || (exports.SocketEventType = {}));
var Socket = /** @class */ (function (_super) {
    tslib_1.__extends(Socket, _super);
    function Socket(id, options) {
        var _this = _super.call(this) || this;
        _this.rooms = {};
        _this.appId = "";
        _this.id = id;
        _this.appId = parseAppId_1.parseAppId(options.appId || "");
        _this.socket = new phoenix_1.Socket(options.url, {
            timeout: options.timeout,
            params: {
                id: id,
            },
        });
        return _this;
    }
    Socket.prototype.connect = function () {
        var _this = this;
        if (this.socket.connectionState() === "open" ||
            this.socket.connectionState() === "connecting") {
            return Promise.resolve(this);
        }
        else {
            return new Promise(function (resolve, reject) {
                _this.socket.onOpen(function () {
                    _this.emit(SocketEventType.CONNECT);
                    resolve(_this);
                });
                _this.socket.onError(reject);
                _this.socket.connect();
            });
        }
    };
    Socket.prototype.disconnect = function (code, reason) {
        var _this = this;
        if (this.socket.connectionState() === "closed" ||
            this.socket.connectionState() === "closing") {
            return Promise.resolve(this);
        }
        else {
            return new Promise(function (resolve, reject) {
                _this.socket.onError(reject);
                _this.socket.disconnect(function () {
                    _this.emit(SocketEventType.CONNECT);
                    resolve(_this);
                }, code, reason);
            });
        }
    };
    Socket.prototype.getId = function () {
        return this.id;
    };
    Socket.prototype.getRoom = function (name) {
        return core_1.Option.from(this.rooms[name]);
    };
    Socket.prototype.getRooms = function () {
        return this.rooms;
    };
    Socket.prototype.getPhoenixSocket = function () {
        return this.socket;
    };
    Socket.prototype.joinRoom = function (name, data, timeout) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var room;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, new Room_1.Room(this, this.socket.channel("room:" + this.appId + "@" + name, { data: data }), name, data).join(timeout)];
                    case 1:
                        room = _a.sent();
                        this.rooms[name] = room;
                        return [2 /*return*/, room];
                }
            });
        });
    };
    Socket.prototype.leaveRoom = function (name, timeout) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var room;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                room = this.getRoom(name);
                if (room.isSome()) {
                    return [2 /*return*/, room
                            .unwrap()
                            .leave(timeout)
                            .then(function () {
                            delete _this.rooms[name];
                            return room;
                        })];
                }
                else {
                    return [2 /*return*/, room];
                }
                return [2 /*return*/];
            });
        });
    };
    return Socket;
}(events_1.EventEmitter));
exports.Socket = Socket;
