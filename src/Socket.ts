import { Option } from "@aicacia/core";
import { EventEmitter } from "events";
import { parseAppId } from "./parseAppId";
import { Socket as PhoenixSocket } from "phoenix";
import { Room } from "./Room";

export interface ISocketOptions {
  url: string;
  appId?: string;
  timeout?: number;
}

export enum SocketEventType {
  CONNECT = "connect",
  DISCONNECT = "disconnect",
}

// tslint:disable-next-line: interface-name
export declare interface Socket {
  on(
    event: SocketEventType.CONNECT | SocketEventType.DISCONNECT,
    listener: (this: Socket) => void
  ): this;
}

export class Socket extends EventEmitter {
  private id: string;
  private socket: PhoenixSocket;
  private rooms: Record<string, Room> = {};
  private appId = "";

  constructor(id: string, options: ISocketOptions) {
    super();
    this.id = id;
    this.appId = parseAppId(options.appId || "");
    this.socket = new PhoenixSocket(options.url, {
      timeout: options.timeout,
      params: {
        id,
      },
    });
  }

  connect(): Promise<this> {
    if (
      this.socket.connectionState() === "open" ||
      this.socket.connectionState() === "connecting"
    ) {
      return Promise.resolve(this);
    } else {
      return new Promise((resolve, reject) => {
        this.socket.onOpen(() => {
          this.emit(SocketEventType.CONNECT);
          resolve(this);
        });
        this.socket.onError(reject);
        this.socket.connect();
      });
    }
  }
  disconnect(code?: number, reason?: string): Promise<this> {
    if (
      this.socket.connectionState() === "closed" ||
      this.socket.connectionState() === "closing"
    ) {
      return Promise.resolve(this);
    } else {
      return new Promise((resolve, reject) => {
        this.socket.onError(reject);
        this.socket.disconnect(
          () => {
            this.emit(SocketEventType.CONNECT);
            resolve(this);
          },
          code,
          reason
        );
      });
    }
  }

  getId() {
    return this.id;
  }

  getRoom<Payload = any, Data = any>(
    name: string
  ): Option<Room<Payload, Data>> {
    return Option.from(this.rooms[name]);
  }
  getRooms() {
    return this.rooms;
  }
  getPhoenixSocket() {
    return this.socket;
  }

  async joinRoom<Payload = any, Data = any>(
    name: string,
    data: Data,
    timeout?: number
  ): Promise<Room<Payload, Data>> {
    const room = await new Room<Payload, Data>(
      this,
      this.socket.channel(`room:${this.appId}@${name}`, { data }),
      name,
      data
    ).join(timeout);
    this.rooms[name] = room;
    return room;
  }

  async leaveRoom<Payload = any, Data = any>(
    name: string,
    timeout?: number
  ): Promise<Option<Room<Payload, Data>>> {
    const room = this.getRoom(name);

    if (room.isSome()) {
      return room
        .unwrap()
        .leave(timeout)
        .then(() => {
          delete this.rooms[name];
          return room;
        });
    } else {
      return room;
    }
  }
}
