import { Channel } from "phoenix";
export declare function channelPush(channel: Channel, event: string, payload: any, timeout?: number): Promise<void>;
