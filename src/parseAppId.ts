const RE_APP_ID = /[^A-Za-z0-9_\-\.]+/gm;

export function parseAppId(appId: string) {
  return appId.replace(RE_APP_ID, "");
}
