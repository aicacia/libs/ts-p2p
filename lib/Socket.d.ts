/// <reference types="node" />
import { Option } from "@aicacia/core";
import { EventEmitter } from "events";
import { Socket as PhoenixSocket } from "phoenix";
import { Room } from "./Room";
export interface ISocketOptions {
    url: string;
    appId?: string;
    timeout?: number;
}
export declare enum SocketEventType {
    CONNECT = "connect",
    DISCONNECT = "disconnect"
}
export declare interface Socket {
    on(event: SocketEventType.CONNECT | SocketEventType.DISCONNECT, listener: (this: Socket) => void): this;
}
export declare class Socket extends EventEmitter {
    private id;
    private socket;
    private rooms;
    private appId;
    constructor(id: string, options: ISocketOptions);
    connect(): Promise<this>;
    disconnect(code?: number, reason?: string): Promise<this>;
    getId(): string;
    getRoom<Payload = any, Data = any>(name: string): Option<Room<Payload, Data>>;
    getRooms(): Record<string, Room<any, any>>;
    getPhoenixSocket(): PhoenixSocket;
    joinRoom<Payload = any, Data = any>(name: string, data: Data, timeout?: number): Promise<Room<Payload, Data>>;
    leaveRoom<Payload = any, Data = any>(name: string, timeout?: number): Promise<Option<Room<Payload, Data>>>;
}
